include(../../../../main.pri)

QT -= core gui
TEMPLATE = lib
INCLUDEPATH += . ../../../../libs/lua ../../../../libs/expat
LIBS += ../../../../libs/expat/libexpat.a -L$$MB_BUILD_DIR/lib -llua
CONFIG += dynamiclib
TARGET = $$MB_BUILD_DIR/share/lib/lua/lxp
QMAKE_LFLAGS += -Wl,-version-script=vscript

SOURCES = lxplib.c

HEADERS = lxplib.h
